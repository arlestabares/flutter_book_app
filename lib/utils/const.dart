const String welcomeToLibrary = 'Bienvenido a su libreria virtual';
const String clickOnTheSearch =
    'Haga click en el buscador y encuentre el mejor libro para usted.';
const String enjoyIt = '...Disfrutelo';
const String enaOfBancolombia = 'Cortesia';
const String courtesyOfBancolombia = 'Ena Teresa Juvinao';
const String bancolombia = 'Bancolombia';
