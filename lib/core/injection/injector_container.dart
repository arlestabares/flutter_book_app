import 'package:flutter_book_app/core/converters/converters.dart';
import 'package:flutter_book_app/features/book_search/data/datasources/remote/book_remote_data_source.dart';
import 'package:flutter_book_app/features/book_search/data/datasources/remote/services/web_service_api.dart';
import 'package:flutter_book_app/features/book_search/data/repositories/book_repository.dart';
import 'package:flutter_book_app/features/book_search/domain/repositories/book_domain_repositoty.dart';
import 'package:flutter_book_app/features/book_search/domain/usecases/book_search_usecase.dart';
import 'package:flutter_book_app/features/book_search/presentation/bloc/book_bloc.dart';
import 'package:flutter_book_app/features/home/data/datasources/remote/book_remote_data_source.dart';
import 'package:flutter_book_app/features/home/data/datasources/remote/services/web_service_api.dart';
import 'package:flutter_book_app/features/home/data/repositories/book_repository.dart';
import 'package:flutter_book_app/features/home/domain/repositories/book_domain_repositoty.dart';
import 'package:flutter_book_app/features/home/domain/usecases/book_search_usecase.dart';
import 'package:flutter_book_app/features/home/presentation/bloc/home_bloc.dart';
import 'package:get_it/get_it.dart';

final sl = GetIt.instance;
Future<void> init() async {
  // Register BookSearchBloc

  sl.registerFactory(
    () => BookSearchBloc(getBookUseCase: sl()),
  );
  sl.registerFactory(
    () => HomeBloc(getBooksUseCase: sl()),
  );
  sl.registerFactory(() => SearchWebServiceApi());
  sl.registerFactory(() => HomeWebServiceApi());
  //Register usecase search
  sl.registerLazySingleton(() => BookSearchByNameUseCase(sl()));
  //Register usecase home
  sl.registerLazySingleton(() => HomeBooksSearchUseCase(sl()));
  //Converts
  sl.registerLazySingleton(() => Converters());
  //Register  Repository search
  sl.registerLazySingleton<BookDomainRepository>(() => BookSearchRepository(sl()));
  //Register  Repository home
  sl.registerLazySingleton<HomeBookDomainRepository>(
      () => HomeBookRepositoryImpl(sl()));
  //Datasources searchBook
  sl.registerLazySingleton<BookRemoteDataSource>(
      () => BookRemoteDataSourceImpl(converters: sl(), webServiceApi: sl()));
  //Datasources home
  sl.registerLazySingleton<HomeBookRemoteDataSource>(() =>
      HomeBookRemoteDataSourceImpl(converters: sl(), webServiceApi: sl()));
  //
}
