import 'package:flutter/material.dart';
import 'package:flutter_book_app/features/home/presentation/pages/home_page.dart';

final Map<String, Widget Function(BuildContext)> appRoutes = {
  'home_page': (_) => const HomePage(),
};
