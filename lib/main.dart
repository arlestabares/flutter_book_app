import 'package:flutter/material.dart';
import 'package:flutter_book_app/core/injection/injector_container.dart' as di;
import 'package:flutter_book_app/features/blocs/multi_bloc_provider.dart';
import 'package:flutter_book_app/routes/routes.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  di.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProviderWidget(
      child: MaterialApp(
        theme: ThemeData(useMaterial3: true),
        title: 'Material App',
        initialRoute: 'home_page',
        routes: appRoutes,
      ),
    );
  }
}
