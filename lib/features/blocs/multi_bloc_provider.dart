import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_book_app/core/injection/injector_container.dart';
import 'package:flutter_book_app/features/book_search/domain/usecases/book_search_usecase.dart';
import 'package:flutter_book_app/features/book_search/presentation/bloc/book_bloc.dart';
import 'package:flutter_book_app/features/home/domain/usecases/book_search_usecase.dart';
import 'package:flutter_book_app/features/home/presentation/bloc/home_bloc.dart';

class MultiBlocProviderWidget extends StatelessWidget {
  const MultiBlocProviderWidget({Key? key, required this.child})
      : super(key: key);
  final Widget child;
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<HomeBloc>(
          create: (context) => HomeBloc(
            getBooksUseCase: sl.get<HomeBooksSearchUseCase>(),
          )..add(
              SearchLastTenBooksReleasedEvent(),
            ),
        ),
        BlocProvider<BookSearchBloc>(
          create: (context) =>
              BookSearchBloc(getBookUseCase: sl.get<BookSearchByNameUseCase>()),
        ),
      ],
      child: child,
    );
  }
}
