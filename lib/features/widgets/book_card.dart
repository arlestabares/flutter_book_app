import 'package:flutter/material.dart';
import 'package:flutter_book_app/core/entities/book_entity.dart';

class BookCard extends StatelessWidget {
  final BookEntity bookEntity;

  const BookCard({
    Key? key,
    required this.bookEntity,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 370.0,
      decoration: const BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            blurRadius: 15.0,
            offset: Offset(-3.0, 10.0),
          )
        ],
        shape: BoxShape.rectangle,
        color: Colors.transparent,
      ),
      margin: const EdgeInsets.fromLTRB(0.0, 8, 10, 5.0),
      child: ClipRRect(
        borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(30.0),
          bottomLeft: Radius.circular(30.0),
        ),
        child: Container(
          color: Colors.white,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Hero(
                tag: bookEntity.title.toString(),
                child: Image.network(
                  '${bookEntity.image}',
                  width: 150,
                  height: 160,
                  fit: BoxFit.fill,
                ),
              ),
              Flexible(
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        bookEntity.title!,
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                        style: const TextStyle(fontSize: 20.0),
                      ),
                      Text(
                        bookEntity.subtitle!,
                        style: const TextStyle(
                          fontWeight: FontWeight.normal,
                          fontSize: 16.0,
                        ),
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        softWrap: false,
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
