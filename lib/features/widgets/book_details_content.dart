import 'package:flutter/material.dart';
import 'package:flutter_book_app/core/entities/book_entity.dart';
import 'package:flutter_book_app/features/book_search/presentation/widgets/hero_widget.dart';
import 'package:url_launcher/url_launcher_string.dart';

class BookDetailsContent extends StatelessWidget {
  const BookDetailsContent({
    Key? key,
    required this.size,
    required this.bookEntity,
  }) : super(key: key);

  final Size size;
  final BookEntity bookEntity;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 12.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: size.height * 0.3,
            width: size.width,
            // color: Colors.black38,
            padding: const EdgeInsets.all(5.0),
            child: HeroWidget(
              tag: 'hero',
              radius: 51.0,
              assetImage: 'assets/images/no-image.jpg',
              networkImage: "${bookEntity.image}",
              fit: BoxFit.cover,
            ),
          ),
          const SizedBox(height: 22.0),
          Text(
            bookEntity.title!,
            style: const TextStyle(
              fontSize: 21.0,
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(height: 12.0),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Text(
                  bookEntity.subtitle!.isEmpty
                      ? 'No description......'
                      : bookEntity.subtitle!,
                  style: TextStyle(
                      fontSize: bookEntity.subtitle!.isEmpty ? 21.0 : 16.0,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
          const SizedBox(height: 8.0),
          Text(
            'ISBN-13:   ${bookEntity.isbn13}',
            style: const TextStyle(fontSize: 18.0),
          ),
          Text(
            'Price: ${bookEntity.price}',
            style: const TextStyle(fontSize: 18.0),
          ),
          TextButton(
              onPressed: () {
                launchUrlString('${bookEntity.url}');
              },
              child: Text('${bookEntity.url}')),
        ],
      ),
    );
  }
}
