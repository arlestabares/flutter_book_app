part of 'book_bloc.dart';

abstract class BookSearchState extends Equatable {
  const BookSearchState(this.model);
  final Model model;

  @override
  List<Object> get props => [model];
}

class BookInitialState extends BookSearchState {
  const BookInitialState(Model model) : super(model);
}

class BookSearchByNameFromApiState extends BookSearchState {
  const BookSearchByNameFromApiState(Model model) : super(model);
}

class BookSearchDetailsState extends BookSearchState {
  const BookSearchDetailsState(Model model) : super(model);
}

class BookSearchErrorState extends BookSearchState {
  const BookSearchErrorState(Model model) : super(model);
}

// ignore: must_be_immutable
class Model extends Equatable {
  final String? query;
  final bool hasReachedMax;
  final BookEntity? bookEntity;
  final List<String>? queryList;
  final BookSearchStatus? status;
  List<BookEntity> bookEntityList;

   Model({
    this.query = '',
    this.bookEntity,
    this.queryList = const <String>[],
    this.hasReachedMax = false,
    this.status = BookSearchStatus.initial,
    this.bookEntityList = const <BookEntity>[],
  });

  Model copiWith({
    String? query,
    bool? hasReachedMax,
    List<String>? queryList,
    BookEntity? bookEntity,
    BookSearchStatus? status,
    List<BookEntity>? bookEntityList,
  }) =>
      Model(
        query: query ?? this.query,
        queryList: queryList ?? this.queryList,
        status: status ?? this.status,
        hasReachedMax: hasReachedMax ?? this.hasReachedMax,
        bookEntity: bookEntity ?? this.bookEntity,
        bookEntityList: bookEntityList ?? this.bookEntityList,
      );

  @override
  List<Object?> get props =>
      [bookEntity, bookEntityList, query, hasReachedMax, queryList];
}

enum BookSearchStatus { initial, success, failure }
