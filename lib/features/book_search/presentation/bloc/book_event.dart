part of 'book_bloc.dart';

abstract class BookSearchEvent extends Equatable {
  const BookSearchEvent();

  @override
  List<Object> get props => [];
}

class BookSearchByNameFromApiEvent extends BookSearchEvent {
  final String bookName;
  final int? page;

  const BookSearchByNameFromApiEvent({required this.bookName, this.page = 1});
}

class BookSearchDetailsEvent extends BookSearchEvent {
  final BookEntity bookEntity;

  const BookSearchDetailsEvent(this.bookEntity);
}
