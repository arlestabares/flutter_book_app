import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_book_app/core/entities/book_entity.dart';
import 'package:flutter_book_app/features/book_search/domain/usecases/book_search_usecase.dart';

part 'book_event.dart';
part 'book_state.dart';

class BookSearchBloc extends Bloc<BookSearchEvent, BookSearchState> {
  final BookSearchByNameUseCase getBookUseCase;
  BookSearchBloc({required this.getBookUseCase}) : super(initialState) {
    on<BookSearchByNameFromApiEvent>(_onBookSearchFromApiEvent);
    on<BookSearchDetailsEvent>(_onBookSearchDetailsEvent);
  }
  static BookSearchState get initialState => BookInitialState(Model());

  _onBookSearchFromApiEvent(
      BookSearchByNameFromApiEvent event, Emitter<BookSearchState> emit) async {
    // var bookSearchCurrentContent = <BookEntity>[];

    if (event.bookName != state.model.query) {
      state.model.bookEntityList = [];
    }
    var queryList = <String>[];

    if (queryList.isEmpty || queryList.length < 6) {
      queryList.add(event.bookName);
    } else {
      queryList.removeAt(0);
      queryList.add(event.bookName);
    }

    final response = await getBookUseCase
        .call(BookParams(bookName: event.bookName, page: event.page!));
        
    response.fold((failure) {
      emit(
        BookSearchErrorState(
            state.model.copiWith(status: BookSearchStatus.failure)),
      );
    }, (data) {
      if (data.isEmpty) {
        emit(BookSearchByNameFromApiState(
          state.model
              .copiWith(status: BookSearchStatus.success, hasReachedMax: true),
        ));
      } else {
        // state.model.bookEntityList! == data;
        for (var element in queryList) {
          if (element.length > 5) {}
        }
        emit(
          BookSearchByNameFromApiState(
            state.model.copiWith(
              status: BookSearchStatus.success,
              // bookEntityList: bookSearchCurrentContent..addAll(data),
              bookEntityList: List.of(state.model.bookEntityList)..addAll(data),
              hasReachedMax: false,
              query: event.bookName,
              queryList: queryList,
            ),
          ),
        );
      }
    });
  }

  _onBookSearchDetailsEvent(
      BookSearchDetailsEvent event, Emitter<BookSearchState> emit) async {
    emit(
      BookSearchDetailsState(
        state.model.copiWith(bookEntity: event.bookEntity),
      ),
    );
  }
}
