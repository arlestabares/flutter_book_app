import 'package:flutter/material.dart';

class HeroWidget extends StatelessWidget {
  const HeroWidget({
    Key? key,
    required this.radius,
    this.networkImage,
    required this.assetImage,
    this.fit,
    this.tag,
  }) : super(key: key);

  final String assetImage;
  final double radius;
  final String? networkImage;
  final BoxFit? fit;
  final Object? tag;
  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: tag!,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(radius),
        child: FadeInImage(
          image: NetworkImage(networkImage ?? ''),
          placeholder: AssetImage(assetImage),
          imageErrorBuilder: (_, __, ___) {
            return Image.asset(assetImage);
          },
          fit: fit,
        ),
      ),
    );
  }
}
