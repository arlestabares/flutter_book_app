import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_book_app/features/book_search/presentation/bloc/book_bloc.dart';
import 'package:flutter_book_app/features/book_search/presentation/views/details/book_search_details_view_content.dart';
import 'package:flutter_book_app/features/book_search/presentation/widgets/book_search_card.dart';
import 'package:flutter_book_app/features/book_search/presentation/widgets/size_transition.dart';
import 'package:flutter_book_app/features/widgets/inkwell_widget.dart';

class BookSearchView extends StatefulWidget {
  const BookSearchView({Key? key, required this.query}) : super(key: key);
  final String query;

  @override
  State<BookSearchView> createState() => _BookSearchViewState();
}

class _BookSearchViewState extends State<BookSearchView> {
  ContainerTransitionType transitionType = ContainerTransitionType.fade;
  final _scrollController = ScrollController();
  int page = 1;
  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_scrollListener);
  }

  @override
  void dispose() {
    _scrollController
      ..removeListener(_scrollListener)
      ..dispose();
    super.dispose();
  }

  _scrollListener() {
    if (_scrollController.position.maxScrollExtent ==
        _scrollController.offset) {
      page++;
      setState(() {});
      context.read<BookSearchBloc>().add(
            BookSearchByNameFromApiEvent(bookName: widget.query, page: page),
          );
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BookSearchBloc, BookSearchState>(
      buildWhen: (_, state) => state is BookSearchByNameFromApiState,
      builder: (context, state) {
        // switch (state.model.status) {
        // case BookSearchStatus.success:
        // if (state.model.bookEntityList!.isEmpty) {
        //   return const Center(
        //     key: Key('No Book'),
        //     child: Card(
        //       child: ListTile(
        //         title: Text('There are not books to show'),
        //       ),
        //     ),
        //   );
        // }

        return ListView.builder(
          controller: _scrollController,
          itemCount: state.model.bookEntityList.length,
          itemBuilder: (context, int index) {
            return InkWellWidget(
              onTap: () => Navigator.push(
                context,
                SizeTransitionRoute(
                  page: BookSearchDetailsViewContent(
                    bookEntity: state.model.bookEntityList[index],
                  ),
                ),
              ),
              child: BookSearchCard(
                bookEntity: state.model.bookEntityList[index],
              ),
            );
          },
        );

        // case BookSearchStatus.failure:
        //   return const Center(
        //     key: Key('failed_to_fetch_book'),
        //     child: Card(
        //       child: ListTile(
        //         title: Text('failed to fetch books'),
        //       ),
        //     ),
        //   );

        // default:
        // return const SizedBox.shrink();
      },
    );
  }
}
