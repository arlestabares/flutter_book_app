import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_book_app/features/book_search/presentation/bloc/book_bloc.dart';
import 'package:flutter_book_app/features/book_search/presentation/views/book_search_view.dart';

class BookSearchDelegate extends SearchDelegate {
  final List<String>? bookList;
  var filterList = <String>[];
  BookSearchDelegate(this.bookList);
  @override
  List<Widget>? buildActions(BuildContext context) {
    return [
      IconButton(
        icon: const Icon(Icons.clear),
        onPressed: () {
          query = '';
        },
      ),
    ];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    return IconButton(
      icon: AnimatedIcon(
        icon: AnimatedIcons.menu_arrow,
        progress: transitionAnimation,
      ),
      onPressed: () {
        query = '';
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return Container();
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    filterList = bookList!.where((element) {
      return element.toLowerCase().contains(query.trim().toLowerCase());
    }).toList();

    if (query.isEmpty || query.length<2) {
      return Container();
    } else {
      context
          .read<BookSearchBloc>()
          .add(BookSearchByNameFromApiEvent(bookName: query));
      return BookSearchView(query: query);
    }
  }
}
