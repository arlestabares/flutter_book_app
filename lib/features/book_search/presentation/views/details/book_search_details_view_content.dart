import 'package:flutter/material.dart';
import 'package:flutter_book_app/core/entities/book_entity.dart';
import 'package:flutter_book_app/features/widgets/book_details_content.dart';

class BookSearchDetailsViewContent extends StatelessWidget {
  const BookSearchDetailsViewContent({Key? key, required this.bookEntity})
      : super(key: key);
  final BookEntity bookEntity;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          '${bookEntity.title}',
          maxLines: 1,
          style: const TextStyle(fontSize: 16.0),
          overflow: TextOverflow.ellipsis,
        ),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios_new),
          onPressed: () => Navigator.pop(context),
        ),
      ),
      body: BookDetailsContent(size: size, bookEntity: bookEntity),
    );
  }
}
