import 'package:flutter_book_app/features/book_search/data/datasources/remote/book_remote_data_source.dart';
import 'package:flutter_book_app/core/entities/book_entity.dart';
import 'package:flutter_book_app/core/errors/failure.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_book_app/features/book_search/domain/repositories/book_domain_repositoty.dart';

class BookSearchRepository implements BookDomainRepository {
  final BookRemoteDataSource bookRemoteDataSource;

  BookSearchRepository(this.bookRemoteDataSource);
  @override
  Future<Either<Failure, List<BookEntity>>> getBook(String bookName, int page) {
    return bookRemoteDataSource.getAllPostFromApi(bookName, page);
  }
}
