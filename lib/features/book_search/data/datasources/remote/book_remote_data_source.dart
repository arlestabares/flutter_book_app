import 'dart:io';

import 'package:dartz/dartz.dart';
import 'package:flutter_book_app/core/converters/converters.dart';
import 'package:flutter_book_app/core/errors/exceptions.dart';
import 'package:flutter_book_app/features/book_search/data/datasources/remote/services/web_service_api.dart';
import 'package:flutter_book_app/core/entities/book_entity.dart';

import '../../../../../core/errors/failure.dart';

abstract class BookRemoteDataSource {
  Future<Either<Failure, List<BookEntity>>> getAllPostFromApi(
      String bookName, int page);
}

class BookRemoteDataSourceImpl implements BookRemoteDataSource {
  final Converters converters;
  final SearchWebServiceApi webServiceApi;

  BookRemoteDataSourceImpl(
      {required this.converters, required this.webServiceApi});
  @override
  Future<Either<Failure, List<BookEntity>>> getAllPostFromApi(
    String text,
    int page,
  ) async {
    try {
      final response = await webServiceApi.getBook(text, page);
      // page++;
      var bookEntityList = <BookEntity>[];
      for (var element in response) {
        bookEntityList.add(converters.bookModelToBookEntity(element));
      }
      return Right(bookEntityList);
    } on ServerException {
      return const Left(ServerFailure('Failed the Server'));
    } on SocketException {
      return const Left(ConnectionFailure('Failed to connect to the network'));
    }
  }
}
