import 'package:flutter_book_app/features/book_search/data/models/book_model.dart';


abstract class ServiceApi {
  Future<List<BookModel>> getBook(String text, int page);
}
