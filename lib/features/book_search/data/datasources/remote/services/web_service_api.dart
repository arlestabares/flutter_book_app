import 'dart:convert';

import 'package:flutter_book_app/core/errors/exceptions.dart';
import 'package:flutter_book_app/features/book_search/data/datasources/remote/services/service_api.dart';
import 'package:flutter_book_app/features/book_search/data/datasources/remote/services/url_paths.dart';
import 'package:flutter_book_app/features/book_search/data/models/book_model.dart';
import 'package:http/http.dart' as http;

class SearchWebServiceApi implements ServiceApi {
  @override
  Future<List<BookModel>> getBook(String bookName, int page) async {
    // final response = await http.get(Uri.https(url, '/users/5'));
    final response = await http.get(Uri.parse(UrlPaths.searchBook(bookName, page)));
    if (response.statusCode == 200) {
      final decodeData = json.decode(response.body);
      // final Iterable jsonToList = decodeData;
      var bookModelList =  BookModelList.fromJsonList (decodeData['books']);
      return bookModelList.books;
    } else {
      throw ServerException();
    }
  }
}
