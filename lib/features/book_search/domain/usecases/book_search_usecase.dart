import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_book_app/core/errors/failure.dart';
import 'package:flutter_book_app/core/usecase/use_case.dart';
import 'package:flutter_book_app/core/entities/book_entity.dart';
import 'package:flutter_book_app/features/book_search/domain/repositories/book_domain_repositoty.dart';

class BookSearchByNameUseCase
    implements IUseCaseCore<List<BookEntity>, BookParams> {
  final BookDomainRepository domainRepository;

  BookSearchByNameUseCase(this.domainRepository);
  @override
  Future<Either<Failure, List<BookEntity>>> call(BookParams param) async {
    return await domainRepository.getBook(param.bookName, param.page);
  }
}

class BookParams extends Equatable {
  final String bookName;
  final int page;

  const BookParams({
    required this.bookName,
    this.page=1,
  });
  @override
  List<Object?> get props => [bookName,page];
}
