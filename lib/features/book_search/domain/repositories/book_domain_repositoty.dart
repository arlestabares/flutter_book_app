import 'package:dartz/dartz.dart';
import 'package:flutter_book_app/core/errors/failure.dart';
import 'package:flutter_book_app/core/entities/book_entity.dart';


abstract class BookDomainRepository {
  Future<Either<Failure, List<BookEntity>>> getBook(String bookName, int page);
}
