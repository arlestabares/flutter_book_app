

import 'package:flutter_book_app/features/home/data/models/book_model.dart';

abstract class HomeServiceApi {
  Future<List<HomeBookModel>> getBook();
}
