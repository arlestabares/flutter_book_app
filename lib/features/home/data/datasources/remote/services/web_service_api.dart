import 'dart:convert';

import 'package:flutter_book_app/core/errors/exceptions.dart';
import 'package:flutter_book_app/features/home/data/datasources/remote/services/service_api.dart';
import 'package:flutter_book_app/features/home/data/datasources/remote/services/url_paths.dart';
import 'package:flutter_book_app/features/home/data/models/book_model.dart';
import 'package:http/http.dart' as http;

class HomeWebServiceApi implements HomeServiceApi {
  @override
  Future<List<HomeBookModel>> getBook() async {
    final response = await http.get(Uri.parse(UrlPaths.searchBook()));
    if (response.statusCode == 200)
     {
      final decodeData = json.decode(response.body);
      // final Iterable jsonToList = decodeData;
      var bookModelList =  HomeBookModelList.fromJsonList (decodeData['books']);
      return bookModelList.books;
    } else {
      throw ServerException();
    }
  }
}
