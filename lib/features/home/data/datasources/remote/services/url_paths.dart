class UrlPaths {
  UrlPaths._constructorPrivate();
  static const String baseUrl = 'https://api.itbook.store/1.0';
  static String searchBook() =>
      '$baseUrl/new';
}
