import 'package:flutter_book_app/core/entities/book_entity.dart';
import 'package:flutter_book_app/core/errors/failure.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_book_app/features/home/data/datasources/remote/book_remote_data_source.dart';
import 'package:flutter_book_app/features/home/domain/repositories/book_domain_repositoty.dart';

class HomeBookRepositoryImpl implements HomeBookDomainRepository {
  final HomeBookRemoteDataSource bookRemoteDataSource;

  HomeBookRepositoryImpl(this.bookRemoteDataSource);

  @override
  Future<Either<Failure, List<BookEntity>>> getBook() {
    return bookRemoteDataSource.getBooksFromApi();
  }
}
