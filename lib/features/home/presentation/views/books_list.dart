import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_book_app/features/book_search/presentation/widgets/size_transition.dart';
import 'package:flutter_book_app/features/home/presentation/bloc/home_bloc.dart';
import 'package:flutter_book_app/features/home/presentation/views/book_details_view.dart';
import 'package:flutter_book_app/features/widgets/book_card.dart';
import 'package:flutter_book_app/features/widgets/inkwell_widget.dart';

class BookListView extends StatelessWidget {
  const BookListView({super.key});

  @override
  Widget build(BuildContext context) {
    final scrollController = ScrollController();
    // if (scrollController.position.) {

    // }
    return BlocBuilder<HomeBloc, HomeState>(
      builder: (context, state) {
        if (state is SearchLastTenBooksReleasedFromApiState) {
          return Container(
            padding: const EdgeInsets.only(top: 6.0),
            height: 180.0,
            child: ListView.builder(
              shrinkWrap: true,
              itemCount: state.model.bookList.length - 1,
              scrollDirection: Axis.horizontal,
              controller: scrollController,
              itemBuilder: (context, int index) {
                return InkWellWidget(
                  onTap: () => Navigator.push(
                    context,
                    SizeTransitionRoute(
                      page: BookDetailsView(
                        bookEntity: state.model.bookList[index],
                      ),
                    ),
                  ),
                  child: BookCard(
                    bookEntity: state.model.bookList[index],
                  ),
                );
              },
            ),
          );
        } else {
          return const SizedBox.shrink();
        }
      },
    );
  }
}
