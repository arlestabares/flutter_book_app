part of 'home_bloc.dart';

abstract class HomeState extends Equatable {
  const HomeState(this.model);
  final Model model;

  @override
  List<Object> get props => [model];
}

class BookInitialState extends HomeState {
  const BookInitialState(Model model) : super(model);
}

class SearchLastTenBooksReleasedFromApiState extends HomeState {
  const SearchLastTenBooksReleasedFromApiState(Model model) : super(model);
}

class BookDetailsState extends HomeState {
  const BookDetailsState(Model model) : super(model);
}

class BooksSearchErrorState extends HomeState {
  const BooksSearchErrorState(Model model) : super(model);
}

// ignore: must_be_immutable
class Model extends Equatable {
  final String? query;
  final bool hasReachedMax;
  final Book? book;
  final List<String>? queryList;
  final BookSearchStatus? status;
  List<BookEntity> bookList;

   Model({
    this.query = '',
    this.book,
    this.queryList = const <String>[],
    this.hasReachedMax = false,
    this.status = BookSearchStatus.initial,
    this.bookList = const <BookEntity>[],
  });

  Model copiWith({
    String? query,
    bool? hasReachedMax,
    List<String>? queryList,
    Book? book,
    BookSearchStatus? status,
    List<BookEntity>? bookList,
  }) =>
      Model(
        query: query ?? this.query,
        queryList: queryList ?? this.queryList,
        status: status ?? this.status,
        hasReachedMax: hasReachedMax ?? this.hasReachedMax,
        book: book ?? this.book,
        bookList: bookList ?? this.bookList,
      );

  @override
  List<Object?> get props =>
      [book, bookList, query, hasReachedMax, queryList];
}

enum BookSearchStatus { initial, success, failure }
