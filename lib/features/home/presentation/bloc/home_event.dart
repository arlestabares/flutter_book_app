part of 'home_bloc.dart';

abstract class HomeEvent extends Equatable {
  const HomeEvent();

  @override
  List<Object> get props => [];
}

class SearchLastTenBooksReleasedEvent extends HomeEvent {}

class BookDetailsEvent extends HomeEvent {
  final Book book;

  const BookDetailsEvent(this.book);
}
