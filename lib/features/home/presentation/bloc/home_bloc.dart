import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_book_app/core/entities/book_entity.dart';
import 'package:flutter_book_app/features/home/domain/entities/book_entity.dart';
import 'package:flutter_book_app/features/home/domain/usecases/book_search_usecase.dart';

part 'home_event.dart';
part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final HomeBooksSearchUseCase getBooksUseCase;
  HomeBloc({required this.getBooksUseCase}) : super(initialState) {
    on<SearchLastTenBooksReleasedEvent>(_onBookSearchFromApiEvent);
    on<BookDetailsEvent>(_onBookSearchDetailsEvent);
  }
  static HomeState get initialState => BookInitialState(Model());

  _onBookSearchFromApiEvent(
      SearchLastTenBooksReleasedEvent event, Emitter<HomeState> emit) async {
    final response = await getBooksUseCase.call(const NoBookParams());

    response.fold((failure) {
      emit(
        BooksSearchErrorState(
            state.model.copiWith(status: BookSearchStatus.failure)),
      );
    }, (data) {
      if (data.isEmpty) {
        emit(SearchLastTenBooksReleasedFromApiState(
          state.model
              .copiWith(status: BookSearchStatus.success, hasReachedMax: true),
        ));
      } else {
        emit(
          SearchLastTenBooksReleasedFromApiState(
            state.model.copiWith(
              status: BookSearchStatus.success,
              // bookEntityList: bookSearchCurrentContent..addAll(data),
              bookList: List.of(state.model.bookList)..addAll(data),
              hasReachedMax: false,
            ),
          ),
        );
      }
    });
  }

  _onBookSearchDetailsEvent(
      BookDetailsEvent event, Emitter<HomeState> emit) async {
    emit(
      BookDetailsState(
        state.model.copiWith(book: event.book),
      ),
    );
  }
}
