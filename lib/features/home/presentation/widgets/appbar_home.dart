import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_book_app/features/book_search/presentation/bloc/book_bloc.dart';
import 'package:flutter_book_app/features/book_search/presentation/views/search_delegate/search_delegate.dart';

AppBar appbarHome(BuildContext context) {
  return AppBar(
    title: const Text('Libreria Virtual'),
    actions: [
      BlocBuilder<BookSearchBloc, BookSearchState>(
        builder: (context, state) {
          return IconButton(
            icon: const Icon(Icons.search),
            onPressed: () {
              showSearch(
                context: context,
                delegate: BookSearchDelegate(state.model.queryList!),
              );
            },
          );
        },
      )
    ],
  );
}
