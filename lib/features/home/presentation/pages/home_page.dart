import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_book_app/features/home/presentation/bloc/home_bloc.dart';
import 'package:flutter_book_app/features/home/presentation/views/books_list.dart';
import 'package:flutter_book_app/features/home/presentation/widgets/appbar_home.dart';
import 'package:flutter_book_app/utils/const.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final style = Theme.of(context)
        .textTheme
        .bodyText1!
        .copyWith(fontWeight: FontWeight.bold);
    return Scaffold(
      appBar: appbarHome(context),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 12.0),
        // color: Colors.red,
        child: Column(
          // mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const SizedBox(height: 21.0),
            Image.asset('assets/images/books-2.jpg'),
            const SizedBox(height: 21.0),
            const Center(child: Text(welcomeToLibrary)),
            const SizedBox(height: 21.0),
            const Center(
                child: Text(
              clickOnTheSearch,
              textAlign: TextAlign.center,
            )),
            const SizedBox(height: 21.0),
            Center(
              child: Text(
                enjoyIt,
                textAlign: TextAlign.center,
                style: style,
              ),
            ),
             const Spacer(),
            // ElevatedButton(
            //   onPressed: () {
            //     context.read<HomeBloc>().add(SearchLastTenBooksReleasedEvent());
            //   },
            //   child: const Text('traer data'),
            // ),
            const BookListView(),
            const Spacer(),
            SafeArea(
              child: Center(
                child: Column(
                  children: [
                    Text(
                      enaOfBancolombia,
                      style: style,
                    ),
                    Text(
                      courtesyOfBancolombia,
                      style: style,
                    ),
                    Text(
                      bancolombia,
                      style: style,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
