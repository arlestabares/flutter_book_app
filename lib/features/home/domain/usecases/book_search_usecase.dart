import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_book_app/core/entities/book_entity.dart';
import 'package:flutter_book_app/core/errors/failure.dart';
import 'package:flutter_book_app/core/usecase/use_case.dart';
import 'package:flutter_book_app/features/home/domain/repositories/book_domain_repositoty.dart';

class HomeBooksSearchUseCase
    implements IUseCaseCore<List<BookEntity>, NoBookParams> {
  final HomeBookDomainRepository domainRepository;

  HomeBooksSearchUseCase(this.domainRepository);
  @override
  Future<Either<Failure, List<BookEntity>>> call(NoBookParams param) async {
    return await domainRepository.getBook();
  }
}

class NoBookParams extends Equatable {
  const NoBookParams();
  @override
  List<Object?> get props => [];
}
