import 'package:dartz/dartz.dart';
import 'package:flutter_book_app/core/entities/book_entity.dart';
import 'package:flutter_book_app/core/errors/failure.dart';


abstract class HomeBookDomainRepository {
  Future<Either<Failure, List<BookEntity>>> getBook();
}
