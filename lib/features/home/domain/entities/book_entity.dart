import 'package:equatable/equatable.dart';

// ignore: must_be_immutable
class Book extends Equatable {
  String? title;
  String? subtitle;
  String? isbn13;
  String? price;
  String? image;
  String? url;
  Book({
    this.title,
    this.subtitle,
    this.isbn13,
    this.price,
    this.image,
    this.url,
  });

  @override
  List<Object?> get props => [title, subtitle, isbn13, price, image, url];
}
